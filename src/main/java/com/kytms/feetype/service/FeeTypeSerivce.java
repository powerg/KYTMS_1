package com.kytms.feetype.service;

import com.kytms.core.model.CommModel;
import com.kytms.core.model.JgGridListModel;
import com.kytms.core.service.BaseService;

import java.util.List;

/**
 * 辽宁捷畅物流有限公司 -信息技术中心
 * 费用类型service
 * @author 臧英明
 * @create 2018-01-04
 */
public interface FeeTypeSerivce<FeeType> extends BaseService<FeeType>  {
    JgGridListModel selectFeeTypeList(CommModel commModel);

    List selectOrderFeeTypeList(CommModel commModel, String where, Object o);

    List<com.kytms.core.entity.FeeType> selectListByHql(String Hql);
}
